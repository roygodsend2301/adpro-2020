package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AcademyServiceImpl implements AcademyService {

    private final AcademyRepository academyRepository;

    private Knight knight;

    public AcademyServiceImpl(AcademyRepository academyRepository) {
        this.academyRepository = academyRepository;
        seed();
    }

    public void seed() {
        academyRepository.addKnightAcademy("Lordran", new LordranAcademy());
        academyRepository.addKnightAcademy("Drangleic", new DrangleicAcademy());
    }


    @Override
    public void produceKnight(String academyName, String knightType) {
        // TODO complete me
<<<<<<< HEAD
        knight  = academyRepository.getKnightAcademyByName(academyName).getKnight(knightType);
=======
>>>>>>> 65f9256b3e12c4929c9fd543c46387b1d80cca53
    }

    @Override
    public List<KnightAcademy> getKnightAcademies() {
        // TODO fix me
<<<<<<< HEAD
        return this.academyRepository.getKnightAcademies();
=======
        return new ArrayList<>();
>>>>>>> 65f9256b3e12c4929c9fd543c46387b1d80cca53
    }

    public Knight getKnight() {
        // TODO fix me
<<<<<<< HEAD
        return this.knight;
=======
        return null;
>>>>>>> 65f9256b3e12c4929c9fd543c46387b1d80cca53
    }
}
