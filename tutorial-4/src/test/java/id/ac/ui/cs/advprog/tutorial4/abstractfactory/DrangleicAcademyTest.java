package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
<<<<<<< HEAD
        drangleicAcademy = new DrangleicAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");
=======
>>>>>>> 65f9256b3e12c4929c9fd543c46387b1d80cca53
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
<<<<<<< HEAD
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(metalClusterKnight instanceof  MetalClusterKnight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
=======
>>>>>>> 65f9256b3e12c4929c9fd543c46387b1d80cca53
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
<<<<<<< HEAD
        assertEquals("Majestic Knight",majesticKnight.getName());
        assertEquals("Metal Cluster Knight", metalClusterKnight.getName());
        assertEquals("Synthetic Knight", syntheticKnight.getName());
=======
>>>>>>> 65f9256b3e12c4929c9fd543c46387b1d80cca53
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
<<<<<<< HEAD
        assertNotNull(majesticKnight.getArmor());
        assertNotNull(majesticKnight.getWeapon());
        assertNotNull(metalClusterKnight.getArmor());
        assertNotNull(metalClusterKnight.getSkill());
        assertNotNull(syntheticKnight.getWeapon());
        assertNotNull(syntheticKnight.getSkill());
=======
>>>>>>> 65f9256b3e12c4929c9fd543c46387b1d80cca53
    }

}
