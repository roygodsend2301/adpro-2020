package id.ac.ui.cs.advprog.tutorial4.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HolyWishTest {

    private Class<?> holyWishClass;
    private HolyWish holyWish;

    @BeforeEach
    public void setUp() throws Exception {
        holyWishClass = Class.forName(HolyWish.class.getName());
        holyWish = HolyWish.getInstance();
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(holyWishClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertFalse(check);
    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
        // TODO create test
<<<<<<< HEAD
        HolyWish holywish1 = HolyWish.getInstance();
        assertEquals(holyWish, HolyWish.getInstance());
        assertEquals(holyWish,holywish1);
=======
>>>>>>> 65f9256b3e12c4929c9fd543c46387b1d80cca53
    }

    @Test
    public void testWish(){
        // TODO create test
<<<<<<< HEAD
        holyWish.setWish("semoga enggak ngulang");
        assertEquals("semoga enggak ngulang",holyWish.getWish());
=======
>>>>>>> 65f9256b3e12c4929c9fd543c46387b1d80cca53
    }

    @Test
    public void testWishExist(){
        // TODO create test
<<<<<<< HEAD
        holyWish.setWish("amin");
        assertEquals("amin",holyWish.getWish());
        assertEquals("amin", holyWish.toString());
        assertEquals(holyWish.getWish(),holyWish.toString());
        assertNotNull(holyWish.getWish());
        assertNotNull(holyWish.toString());
=======
>>>>>>> 65f9256b3e12c4929c9fd543c46387b1d80cca53
    }

}
