package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

<<<<<<< HEAD
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
=======
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
>>>>>>> 65f9256b3e12c4929c9fd543c46387b1d80cca53
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

<<<<<<< HEAD
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;


=======
>>>>>>> 65f9256b3e12c4929c9fd543c46387b1d80cca53
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

<<<<<<< HEAD
    private AcademyRepository academyRepository;
    private AcademyService academyService;

    // TODO create tests
    @BeforeEach
    public void setUp(){
        academyRepository = new AcademyRepository();
        academyService = new AcademyServiceImpl(academyRepository);
    }

    @Test
    public void testInitialAmountOfKnightAcademyShouldBe2(){
        assertEquals(2, academyService.getKnightAcademies().size());
    }

    @Test
    public void testMajesticKnightProduction(){
        academyService.produceKnight("Drangleic","majestic");
        Knight knight = academyService.getKnight();
        assertNotNull(knight);
        assertEquals("Majestic Knight",knight.getName());
        academyService.produceKnight("Lordran","majestic");
        knight = academyService.getKnight();
        assertNotNull(knight);
        assertEquals("Majestic Knight",knight.getName());
    }

    @Test
    public void testMetalClusterKnightProduction(){
        academyService.produceKnight("Drangleic","metal cluster");
        Knight knight = academyService.getKnight();
        assertNotNull(knight);
        assertEquals("Metal Cluster Knight",knight.getName());
        academyService.produceKnight("Lordran","metal cluster");
        knight = academyService.getKnight();
        assertNotNull(knight);
        assertEquals("Metal Cluster Knight",knight.getName());
    }

    @Test
    public void testSyntheticKnightProduction(){
        academyService.produceKnight("Drangleic","synthetic");
        Knight knight = academyService.getKnight();
        assertNotNull(knight);
        assertEquals("Synthetic Knight",knight.getName());
        academyService.produceKnight("Lordran","synthetic");
        knight = academyService.getKnight();
        assertNotNull(knight);
        assertEquals("Synthetic Knight",knight.getName());
    }

=======
    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyService academyService;

    // TODO create tests
>>>>>>> 65f9256b3e12c4929c9fd543c46387b1d80cca53
}
