package id.ac.ui.cs.advprog.tutorial5.controller;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulService;
import id.ac.ui.cs.advprog.tutorial5.service.SoulServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// TODO: Import apapun yang anda perlukan agar controller ini berjalan
@RestController
@RequestMapping(path = "/soul")
public class SoulController {

    @Autowired
    private SoulService soulService;

    @GetMapping
    public ResponseEntity<List<Soul>> findAll() {
        return new ResponseEntity<List<Soul>>(soulService.findAll(), HttpStatus.OK);
        // TODO: Use service to complete me.
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Soul soul) {
        soulService.register(soul);
        return new ResponseEntity(HttpStatus.OK);
        // TODO: Use service to complete me.
    }

    @GetMapping("/{id}")
    public ResponseEntity<Soul> findById(@PathVariable Long id) {
        // TODO: Use service to complete me.
        if (soulService.findSoul(id).isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else{
            return new ResponseEntity<Soul>(soulService.findSoul(id).get(), HttpStatus.OK);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Soul> update(@PathVariable Long id, @RequestBody Soul soul) {
        // TODO: Use service to complete me.
        if (soulService.findSoul(id).isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else{
            soul.setId(id);
            return new ResponseEntity<Soul>(soulService.rewrite(soul), HttpStatus.OK);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        // TODO: Use service to complete me.
        if (soulService.findSoul(id).isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else{
            soulService.erase(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }
}
