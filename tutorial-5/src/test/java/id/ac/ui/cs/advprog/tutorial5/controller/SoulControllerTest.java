package id.ac.ui.cs.advprog.tutorial5.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.junit.jupiter.api.Assertions.*;


import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultHandler;

@WebMvcTest(controllers = SoulController.class)
class SoulControllerTest {
    @Autowired
    public MockMvc mockito;

    @MockBean
    private SoulServiceImpl soulService;

    @Test
    public void testFindAll() throws Exception{
        mockito.perform(get("/soul"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(new ResultHandler() {
                    @Override
                    public void handle(MvcResult result) throws Exception {
                        assertEquals("[]",result.getResponse().getContentAsString());
                    }
                });

    }
    @Test
    public void testCreate() throws Exception{
        Soul soul = new Soul();
        soul.setAge(20);
        soul.setGender("Male");
        soul.setName("Roy");
        soul.setOccupation("Data Scientist");
        soul.setId(1);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(soul);

        mockito.perform(post("/soul")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testFindById() throws Exception{
        mockito.perform(get("/soul/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdate() throws Exception{
        Soul soul = new Soul();
        soul.setAge(12);
        soul.setGender("M");
        soul.setName("Testing");
        soul.setOccupation("Testing");
        soul.setId(1);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(soul);

        mockito.perform(put("/soul/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isNotFound());

    }

    @Test
    public void testDelete() throws Exception {
        mockito.perform(delete("/soul/1"))
                .andExpect(status().isNotFound());
    }




}
