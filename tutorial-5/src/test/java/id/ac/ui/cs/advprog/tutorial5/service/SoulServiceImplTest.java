package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SoulServiceImplTest {

    @Mock
    private SoulRepository soulRepository;

    private Soul soul;

    @InjectMocks
    private SoulServiceImpl soulService;

    @BeforeEach
    public void setUp(){
        soul = new Soul(1, "Roy", 20, "M", "Mahasiswa");
    }

    @Test
    public void testFindAll(){
        List<Soul> soulList = soulService.findAll();
        lenient().when(soulService.findAll()).thenReturn(soulList);
    }

    @Test
    public void testRegisterSoul(){
        soulService.register(soul);
        lenient().when(soulService.register(soul)).thenReturn(soul);
    }

    @Test
    public void testFindSoul(){
        soulService.register(soul);
        Optional<Soul> optionalSoulExist = soulService.findSoul(soul.getId());
        Optional<Soul> optionalSoulDoesntExist = soulService.findSoul((long) 2);
        lenient().when(soulService.findSoul(soul.getId())).thenReturn(Optional.of(soul));
    }
    @Test
    public void testErase(){
        soulService.register(soul);
        soulService.erase(soul.getId());
        lenient().when(soulService.findSoul(soul.getId())).thenReturn(Optional.empty());
    }
    @Test
    public void testRewriteSoul(){
        soulService.register(soul);
        assertEquals(soul,soulService.rewrite(soul));
    }

    @Test
    public void whenFindAllIsCalledItShouldCallSoulRepositoryFindAll() {
        soulService.findAll();

        verify(soulRepository, times(1)).findAll();
    }

    @Test
    public void whenFindSoulIsCalledItShouldCallSoulRepositoryFindById() {
        soulService.findSoul((long) 1);

        verify(soulRepository, times(1)).findById((long) 1);
    }

    @Test
    public void whenEraseIsCalledItShouldCallSoulRepositoryDeleteById() {
        soulService.erase((long) 1);

        verify(soulRepository, times(1)).deleteById((long) 1);
    }

    @Test
    public void whenRewriteIsCalledItShouldCallSoulRepositorySave() {
        Soul soul1 = new Soul(24, "Roy", 20, "M", "Mahasiswa");

        soulService.rewrite(soul1);

        verify(soulRepository, times(1)).save(soul1);
    }

    @Test
    public void whenRegisterIsCalledItShouldCallSoulRepositorySave() {
        Soul newSoul = new Soul(24, "Roy", 17, "M", "Mahasiswa");

        soulService.register(newSoul);

        verify(soulRepository, times(1)).save(newSoul);
    }

}
