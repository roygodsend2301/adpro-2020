package id.ac.ui.cs.advprog.tutorial5.core;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SoulTest {

    private Soul soul;

    @BeforeEach
    public void setUp() throws Exception {
        this.soul = new Soul(24, "Roy", 20, "M", "Mahasiswa");
    }

    @Test
    public void testGetName() {
        assertEquals("Roy",soul.getName());
    }

    @Test
    public void testGetAge() {
        assertEquals(20, soul.getAge());
    }

    @Test
    public void testGetGender() {
        assertEquals("M",soul.getGender());
    }

    @Test
    public void testGetOccupation() {
        assertEquals("Mahasiswa", soul.getOccupation());
    }

    @Test
    public void testsetName() {
        soul.setName("sukarno");
        assertEquals("sukarno", soul.getName());
    }

    @Test
    public void testsetGender() {
        soul.setGender("FM");
        assertEquals("FM", soul.getGender());
    }

    @Test
    public void testsetAge() {
        soul.setAge(18);
        assertEquals(18, soul.getAge());
    }
}

