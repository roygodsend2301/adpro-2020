package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                //ToDo: Complete Me
                this.guild= guild;
                guild.add(this);
        }

        //ToDo: Complete Me

        @Override
        public void update() {
                String quest = guild.getQuestType();
                if(quest.equals("D") || quest.equals("R")){
                        getQuests().add(guild.getQuest());
                }
        }
}
