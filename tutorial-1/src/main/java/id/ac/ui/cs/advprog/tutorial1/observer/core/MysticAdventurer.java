package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                //ToDo: Complete Me
                this.guild = guild;
                guild.add(this);
        }

        //ToDo: Complete Me

        @Override
        public void update() {
                String quest = guild.getQuestType();
                if(quest.equals("D") || quest.equals("E")){
                        getQuests().add(guild.getQuest());
                }
        }
}
