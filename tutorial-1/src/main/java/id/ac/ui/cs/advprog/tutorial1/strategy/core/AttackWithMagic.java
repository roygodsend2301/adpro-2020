package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
        //ToDo: Complete me

    @Override
    public String attack() {
        return "Magic Attack";
    }

    @Override
    public String getType() {
        return "Magic";
    }
}
