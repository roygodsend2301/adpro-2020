package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;

    public RawUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName() + " with raw upgrade";
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        Random randomizer = new Random();
        int weaponVal = weapon.getWeaponValue() + randomizer.nextInt(6) +5 ;
        //TODO: Complete me
        return weaponVal ;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription() + " with raw upgrade";
    }
}
