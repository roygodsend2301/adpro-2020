package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    public ChaosUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName() + " with chaos upgrade";
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        Random randomizer = new Random();
        int weaponVal = this.weapon.getWeaponValue() + 50 + randomizer.nextInt(6);
        //TODO: Complete me
        return weaponVal;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription() + " with chaos upgrade";
    }
}
