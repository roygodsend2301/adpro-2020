package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;
    public RegularUpgrade(Weapon weapon) {
        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName() + " with regular upgrade";
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        Random randomizer = new Random();
        int weaponValue = weapon.getWeaponValue() + randomizer.nextInt(6);
        //TODO: Complete me
        return weaponValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription() + " with regular upgrade";
    }
}
