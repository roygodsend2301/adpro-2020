package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
        //TODO: Complete me
    public String name, role;
    public ArrayList<Member> members;
    public OrdinaryMember(String name, String role){
        this.name = name;
        this.role = role;
        this.members = new ArrayList<Member>();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void addChildMember(Member member) {
        // Do Nothing
    }

    @Override
    public void removeChildMember(Member member) {
        // Do Nothing
    }

    @Override
    public List<Member> getChildMembers() {
        return members;
    }

    @Override
    public String getRole() {
        return this.role;
    }
}
