package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;
    int weaponValue;
    public UniqueUpgrade(Weapon weapon){

        this.weapon = weapon;
    }

    @Override
    public String getName() {
        return weapon.getName() + " with unique upgrade";
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        Random randomizer = new Random();
        int weaponValue = weapon.getWeaponValue() + 10 +randomizer.nextInt(6);
        //TODO: Complete me
        return weaponValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription() + " with unique upgrade";
    }
}
