package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals("Aqua", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Goddess", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        Member kamu = new OrdinaryMember("orang","ganteng");
        member.addChildMember(kamu);
        assertEquals(1,member.getChildMembers().size()  );
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member kamu = new OrdinaryMember("orang","ganteng");
        member.addChildMember(kamu);
        member.removeChildMember(kamu);
        assertEquals(0,member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member aku = new OrdinaryMember("orang","ganteng");
        Member kamu = new OrdinaryMember("orang","ganteng");
        Member dia = new OrdinaryMember("orang","ganteng");
        Member ayahmu = new OrdinaryMember("orang","ganteng");

        member.addChildMember(aku);
        member.addChildMember(kamu);
        member.addChildMember(dia);
        member.addChildMember(ayahmu);

        assertEquals(3, member.getChildMembers().size() );
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member master = new PremiumMember("orang","master");

        Member aku = new OrdinaryMember("orang","ganteng");
        Member kamu = new OrdinaryMember("orang","ganteng");
        Member dia = new OrdinaryMember("orang","ganteng");
        Member ayahmu = new OrdinaryMember("orang","ganteng");

        master.addChildMember(aku);
        master.addChildMember(kamu);
        master.addChildMember(dia);
        master.addChildMember(ayahmu);

        assertEquals(4,master.getChildMembers().size());
    }
}
